use strict;
use warnings;

sub main {
    #this is a scalar value
    my $variable = 42;
    print "hello world $variable";

    #escaping double quotes
    print("hi wie gehts \"gut\"");
    print(qq{spare me the "escaping!"});

    my $stringWithWhiteSpace = 'foo bar baz';

    #string is going to be replaced
    $stringWithWhiteSpace =~ s/ /, /g;
    print $stringWithWhiteSpace;

    print replaceAnyWhiteSpaceWithWhiteSpaceAndComma('JEANLUC Picard');
    # watch next: https://youtu.be/WEghIXs8F6c?t=2076
}

sub replaceAnyWhiteSpaceWithWhiteSpaceAndComma {
    my ($toBeReplaced) = @_;
    $toBeReplaced =~ s/ /, /g;
    return $toBeReplaced;
}

main()